<?php 
	require_once('../../private/initialize.php');

	if (!isset($_GET['person_id'])) {
		redirect_to(url_for('/guest/person_index_guest_view.php'));
	}

	$person_id = $_GET['person_id'];
	$person = find_person_by_id($person_id);
	$congregation = find_congregation_by_id($person['reg_congregation_id']);
	$congregation_name = $congregation['congregation_name'];

	// $congregations = find_all_congregations();

	$attendance_list = find_attendance_by_person_id($person_id);

	$page_title = 'View Attendance By Person';
	include(SHARED_PATH . '/header.php'); 
?>

<!-- debug -->
<!-- <?php print_r($attendance_list); ?> -->

<!-- personal info -->
<div class="container-fluid bg-light">
	<h4>Person Info</h4>
	<br>

	<div class="row">
		<div class="col-sm-2"><label class="font-weight-bold">Full Name:</label><br><?php echo h(replace_empty_string($person['full_name'])); ?></div>
		<div class="col-sm-2"><label class="font-weight-bold">Prefered Name:&nbsp;</label><br><?php echo h(replace_empty_string($person['prefered_name'])); ?></div>
		<div class="col-sm-2"><label class="font-weight-bold">Gender:&nbsp;</label><br><?php echo get_gender_txt(h($person['gender'])); ?></div>
		<div class="col-sm-2"><label class="font-weight-bold">Spiritual Status:&nbsp;</label><br><?php echo get_spiritual_status_txt(h($person['spiritual_status'])); ?></div>
		<div class="col-sm-2"><label class="font-weight-bold">Congregation:&nbsp;</label><br><?php echo h(replace_empty_string($congregation_name)); ?></div>

	</div>
</div>
<br>

<!-- attendance info -->
<div class="container-fluid bg-light">
	<h4>Attendance Info</h4>
	<br>

	<div class="container-fluid border border-top-0 border-right-0 border-left-0">
		<?php 
			foreach ($attendance_list as $attendance) {
				echo "<div class=\"row\">";
				echo 		"<div class=\"col-sm-2\"><label class=\"font-weight-bold ";
					switch ($attendance['attendance_code']) {
						case '2': echo " text-warning"; break;
						case '3': echo " text-danger"; break;
					}
				echo    "\">" . h(get_attendance_code_txt($attendance['attendance_code'])) . "</label></div>";
				echo 		"<div class=\"col-sm-2\">" . h(replace_empty_string($attendance['event_name'])) . "</div>";
				echo 		"<div class=\"col-sm-2\">" . h(replace_empty_string($attendance['event_venue'])) . "</div>";
				echo 		"<div class=\"col-sm-2\">" . h(replace_empty_string($attendance['event_begin_datetime'])) . "</div>";
				echo 		"<div class=\"col-sm-2\">" . h(replace_empty_string($attendance['host_congregation_name'])) . "</div>&nbsp;";
				echo "</div>";
			}
		?>
	</div>
</div>


<?php include(SHARED_PATH . '/footer.php'); ?>
