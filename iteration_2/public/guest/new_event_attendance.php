<?php 
	require_once('../../private/initialize.php');

	if (!isset($_GET['event_id']) || empty(find_event_by_id($_GET['event_id']))) {
		redirect_to(url_for('/guest/view_event_list.php')); 
	}

	$event_id = $_GET['event_id'];
	$event = find_event_by_id($_GET['event_id']);
	$host_congregation = find_congregation_by_id($event['host_congregation_id']);
	$host_congregation_name = $host_congregation['congregation_name'];

	//insert or update attendance record for one person
	if (isset($_GET['event_id']) && isset($_GET['person_id']) && isset($_GET['attendance_code'])) {
		//update attenance info in db before retrieving from db
		$attenance = [];
		$attenance['event_id'] = $event_id;
		$attenance['person_id'] = $_GET['person_id'];
		$attenance['attendance_code'] = $_GET['attendance_code'];
		$attenance['comment'] = '';
		if(empty(find_attendance_by_event_id_person_id($attenance))) {
			insert_attendance($attenance);//attendance record does not exist, insert
		} else {
			update_attendance($attenance);//attendance record already exist, update
		}
		redirect_to(url_for('/guest/new_event_attendance.php?event_id=' . $event_id));
	}

	//insert attendance records for all remaining regular attendees when click no more regular button
	if (isset($_GET['no_more_regular'])) {
		//find all the current regular attendees
		$regular_attendees = find_all_person_attendance_by_event($event_id, false);
		foreach ($regular_attendees as $regular_attendee) {
			if (empty($regular_attendee['attendance_code'])) {
				$attenance = [];
				$attenance['event_id'] = $event_id;
				$attenance['person_id'] = $regular_attendee['person_id'];
				$attenance['attendance_code'] = 3;//set attendance_code to absent
				$attenance['comment'] = '';
				if(empty(find_attendance_by_event_id_person_id($attenance))) {
					insert_attendance($attenance);//attendance record does not exist, insert
				} else {
					update_attendance($attenance);//attendance record already exist, update
				}
			}
		}
	}
	
	//get the latest attendance record for all the regular attendees
	$has_regular_not_set = false;
	$regular_attendees = find_all_person_attendance_by_event($event_id, false);
	foreach ($regular_attendees as $regular_attendee) {
		if (empty($regular_attendee['attendance_code']) || $regular_attendee['attendance_code'] == '4') {
			$has_regular_not_set = true;
		}
	}

	//insert attendance records for all remaining visitors when click no more visitor button
	if (isset($_GET['no_more_visitor'])) {
		$visitors = find_all_person_attendance_by_event($event_id, true);
		foreach ($visitors as $visitor) {
			if ($visitor['attendance_code'] != '1' && $visitor['attendance_code'] != '2') {
				$attenance = [];
				$attenance['event_id'] = $event_id;
				$attenance['person_id'] = $visitor['person_id'];
				$attenance['attendance_code'] = 4;//set attendance_code to unknown
				$attenance['comment'] = '';
				if(empty(find_attendance_by_event_id_person_id($attenance))) {
					insert_attendance($attenance);//attendance record does not exist, insert
				} else {
					update_attendance($attenance);//attendance record already exist, update
				}
			}
		}
	}

	$has_visitor_attended = false;
	$has_visitor_not_set = false;
	$visitors = find_all_person_attendance_by_event($event_id, true);
	foreach ($visitors as $visitor) {
		if (!empty($visitor['attendance_code']) && $visitor['attendance_code'] != '4') {
			$has_visitor_attended = true;
		}
		if (empty($visitor['attendance_code'])) {
			$has_visitor_not_set = true;
		} 
	}

	$page_title = 'Record Attendance';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">

	<h4>Record Attendance</h4>
	<br>

	<h5>Event Detail</h5>
	<table class="table">
		<tr class="d-flex">
			<td class="col-4"><?php echo h(replace_empty_string($event['event_name'])); ?></td>
			<td class="col-4"><?php echo h(replace_empty_string($event['event_venue'])); ?></td>
			<td class="col-4"><?php echo h($event['event_begin_datetime']); ?></td>
		</tr>
	</table>
	<br>
	
	<h5>
		Record Regular Attendees &nbsp;
		<?php 
			if ($has_regular_not_set) {
				echo "<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=' . $event_id . '&no_more_regular=true') . "\"><button class=\"btn btn-primary btn-sm\">Mark remaining regular attendees as absent</button></a>";
			}
		?>
	</h5>
	<table class="table	table-striped table-condensed bg-basic">
		<tr class="d-flex">
			<th class="col-4">Prefered Name</th>
			<th class="col-4">Previous attendance</th>
			<th class="col-4">Current Attendance</th>
		</tr>
		<?php 
			foreach ($regular_attendees as $regular_attendee) {
				echo "<tr class=\"d-flex\">";
					echo "<td class=\"col-4\">" . h(replace_empty_string($regular_attendee['prefered_name'])) . "</td>";
					//TODO: previous attendance situation
					echo "<td class=\"col-4\">" . 
									"<button class=\"btn btn-success disabled btn-sm\">3W</button>&nbsp;" . 
									"<button class=\"btn btn-warning disabled btn-sm\">2W</button>&nbsp;" . 
									"<button class=\"btn btn-danger disabled btn-sm\">1W</button>&nbsp;" . 
							 "</td>";
					echo "<td class=\"col-4\">";
					if (!isset($regular_attendee['attendance_code']) || $regular_attendee['attendance_code'] == '0') {
						echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $regular_attendee['person_id'] . "&attendance_code=" . 1 . "\"><button class=\"btn btn-success btn-sm\">" . get_attendance_code_txt('1') . "</button></a> ";
						echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $regular_attendee['person_id'] . "&attendance_code=" . 2 . "\"><button class=\"btn btn-warning btn-sm\">" . get_attendance_code_txt('2') . "</button></a> ";
						echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $regular_attendee['person_id'] . "&attendance_code=" . 3 . "\"><button class=\"btn btn-danger btn-sm\">" . get_attendance_code_txt('3') . "</button></a> ";
						echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $regular_attendee['person_id'] . "&attendance_code=" . 0 . "\"><button class=\"btn btn-sm\">" . get_attendance_code_txt('0') . "</button></a> ";
					} 
					else {
						echo "<button class=\"btn "; 
							switch ($regular_attendee['attendance_code']) {
								case '1': echo "btn-success"; break;
								case '2': echo "btn-warning"; break;
								case '3': echo "btn-danger"; break;
								default: break;
							}
						echo " btn-sm\" disabled>" . get_attendance_code_txt($regular_attendee['attendance_code']) . "</button>";
					}
					echo "</td>";
				echo "</tr>";
			}
		?>
	</table>
	<br>

	<h5>
		Record Visitors &nbsp;
		<?php 
			if ($has_visitor_not_set) {
				echo "<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=' . $event_id . '&no_more_visitor=true') . "\"><button class=\"btn btn-primary btn-sm\">No more visitor to record</button></a>";
			}
		?>
	</h5>
	<table class="table	table-striped table-condensed bg-basic">
		<tr class="d-flex">
			<th class="col-4">Prefered Name</th>
			<th class="col-4">Attendance</th>
		</tr>
		<?php 
			if (!$has_visitor_not_set && !$has_visitor_attended) {
				echo "<td class=\"col-4\">Attendance record finished, no visitor this time</td>";
				echo "<td class=\"col-4\"></td>";
			} else {
				foreach ($visitors as $visitor) {
					echo "<tr class=\"d-flex\">";
						echo "<td class=\"col-4\">" . h(replace_empty_string($visitor['prefered_name'])) . "</td>";
						echo "<td class=\"col-4\">";
							if ($visitor['attendance_code'] != '4') {
								if (!isset($visitor['attendance_code'])) {
									echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $visitor['person_id'] . "&attendance_code=" . '1' . "\"><button class=\"btn btn-success btn-sm\">" . get_attendance_code_txt('1') . "</button></a> ";
									echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $visitor['person_id'] . "&attendance_code=" . '2' . "\"><button class=\"btn btn-warning btn-sm\">" . get_attendance_code_txt('2') . "</button></a> ";
									echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $visitor['person_id'] . "&attendance_code=" . '2' . "\"><button class=\"btn btn-danger btn-sm\">" . get_attendance_code_txt('3') . "</button></a> ";
									echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $visitor['person_id'] . "&attendance_code=" . '0' . "\"><button class=\"btn btn-sm\">" . get_attendance_code_txt('0') . "</button></a> ";
								} 
								else {
									echo "<button class=\"btn "; 
									switch ($visitor['attendance_code']) {
										case '1': echo "btn-success"; break;
										case '2': echo "btn-warning"; break;
										case '3': echo "btn-danger"; break;
										default: break;
									}
									echo " btn-sm\" disabled>" . get_attendance_code_txt($regular_attendee['attendance_code']) . "</button>";
								}
							}
						echo "</td>";
					echo "</tr>";
					}
				}
		?>
	</table>
	<br>

</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
