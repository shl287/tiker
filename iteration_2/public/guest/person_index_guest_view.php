<?php 
	require_once('../../private/initialize.php');

	$congregations = find_all_congregations();
	$persons = find_all_persons();

	$page_title = 'Person List';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">
	<h4>Person List</h4>
	<br>

	<table class="table	table-striped table-condensed table-bordered bg-basic">
		<tr>
			<th>&nbsp;</th>
			<th>Prefered Name</th>
			<th>Regular Congregation</th>
		</tr>

		<?php 
			foreach ($persons as $person) {
				echo "<tr>";
				echo "<td><a href=\"" . url_for('/guest/view_attendance_by_person.php?person_id=') . h(u($person['person_id'])) . "\"><button class=\"btn btn-primary btn-sm\">View Attendance</button></a></td>";
				echo "<td>" . h(replace_empty_string($person['prefered_name'])) . "</td>";
				foreach ($congregations as $congregation) {
					if ($person['reg_congregation_id'] === $congregation['congregation_id']) {
						echo "<td>" . h($congregation['congregation_name']) . "</td>";
					}
				}
				echo "</tr>";
			}
		?>
	</table>
	<br>

</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
