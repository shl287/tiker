<?php require_once('../private/initialize.php'); ?>

<?php $page_title = 'Guest Menu'; ?>

<?php include(SHARED_PATH . '/header.php'); ?>

<div class="container-fluid bg-light">
	<div class="row bg-light">
		<br>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-success text-center">
        <div class="panel panel-heading">
          <h4>Tik Attendance</h4>
        </div><!-- panel heading -->
				<div class="panel panel-body">
					<a href="<?php echo url_for('/guest/view_event_list.php'); ?>"><button type="button" class="btn btn-primary btn-sm btn-block">By Event</button></a>
					<br>
					<!-- <a href="<?php echo url_for(''); ?>"><button type="button" class="btn btn-primary btn-sm btn-block disabled">By Person (disabled)</button></a> -->
					<p>more features coming soon...</p>
				</div><!-- panel body -->
			</div><!-- panel -->
			<br>
		</div><!-- col -->
		
		<div class="col-md-4">
			<div class="panel panel-success text-center">
        <div class="panel panel-heading">
					<h4>Analyse Attendance</h4>
				</div><!-- panel heading -->
				<div class="panel panel-body">
					<a href="<?php echo url_for('/guest/person_index_guest_view.php'); ?>"><button type="button" class="btn btn-primary btn-sm btn-block">By Person</button></a>
					<br>
					<!-- <a href="<?php echo url_for(''); ?>"><button type="button" class="btn btn-primary btn-sm btn-block disabled">By Congregation (disabled)</button></a> -->
					<p>more features coming soon...</p>
				</div>
			</div><!-- panel -->
			<br>
		</div>

		<div class="col-md-4">
			<div class="panel panel-success text-center">
        <div class="panel panel-heading">
          <h4>Admin</h4>
        </div><!-- panel heading -->
				<div class="panel panel-body">
					<a href="<?php echo url_for('/admin/congregation/congregation_index.php'); ?>"><button type="button" class="btn btn-primary btn-sm btn-block">Manage Congregation</button></a>
					<br>
					<a href="<?php echo url_for('/admin/event/event_index.php'); ?>"><button type="button" class="btn btn-primary btn-sm btn-block">Manage Event</button></a>
					<br>
					<a href="<?php echo url_for('/admin/person/person_index_admin_view.php'); ?>"><button type="button" class="btn btn-primary btn-sm btn-block">Manage Person</button></a>
					<br>
					<p>more features coming soon...</p>
				</div><!-- panel body -->
			</div><!-- panel -->
		</div><!-- col -->

	</div><!-- row -->
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
