<?php 
	require_once('../../../private/initialize.php');

	if (!isset($_GET['event_id'])) {
		redirect_to(url_for('/admin/event/event_index.php'));
	}

	$event_id = $_GET['event_id'] ?? $_POST['event_id'];
	$event = find_event_by_id($event_id);
	$congregation = find_congregation_by_id($event['host_congregation_id']);
	$congregation_name = $congregation['congregation_name'];

	//deal with form submission
	if(is_post_request()) {
		$result = delete_event_by_id($event_id);
		if ($result === true) {
			redirect_to(url_for('/admin/event/event_index.php'));
		}
	}

	$page_title = 'Delete Event';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">

	<h4>Are you sure you want to delete this event?</h4>
	<br>

	<dl>
		<dt>Event Name</dt>
		<dd><?php echo h(replace_empty_string($event['event_name'])); ?></dd>
	</dl>
	<dl>
		<dt>Venue</dt>
		<dd><?php echo h(replace_empty_string($event['event_venue'])); ?></dd>
	</dl>
	<dl>
		<dt>Congregation</dt>
		<dd><?php echo h(replace_empty_string($congregation_name)); ?>
		</dd>
	</dl>
	<dl>
		<dt>Datetime</dt>
		<dd><?php echo h($event['event_begin_datetime']); ?>
		</dd>
	</dl>
	<br>

	<form action="<?php echo url_for('/admin/event/delete_event.php?event_id=' . h(u($event_id))) ?>" method="post">
		<input type="submit" name="commit" value="Delete" class="btn btn-primary btn-sm">
	</form>
	<br>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
