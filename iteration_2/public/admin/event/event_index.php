<?php 
	require_once('../../../private/initialize.php');

	$events = find_all_events();
	$congregations = find_all_congregations();

	$page_title = 'Event Management';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">
	<h4>Event List &nbsp; <a href="<?php echo url_for('/admin/event/new_event.php') ?>"><button class="btn btn-primary btn-sm">Register New Event</button></a></h4>
	<br>
	
	<table class="table table-striped table-condensed table-bordered bg-basic">
		<tr>
			<th>&nbsp;</th>
			<th>Event Name</th>
			<th>Venue</th>
			<!-- <th>Host Congregation</th> -->
			<th>Event Datetime</th>
		</tr>
		<?php 
			foreach ($events as $event) {
				echo "<tr>";
				echo "<td><a href=\"" . url_for('/admin/event/show_event.php?event_id=') . h(u($event['event_id'])) . "\"><button class=\"btn btn-primary btn-sm\">View</button></a>";
				echo "		<a href=\"" . url_for('/admin/event/edit_event.php?event_id=') . h(u($event['event_id'])) . "\"><button class=\"btn btn-primary btn-sm\">Edit</button></a>";
				echo "		<a href=\"" . url_for('/admin/event/delete_event.php?event_id=') . h(u($event['event_id'])) . "\"><button class=\"btn btn-primary btn-sm\">Delete</button></a></td>";
				echo "<td>" . h(replace_empty_string($event['event_name'])) . "</td>";
				echo "<td>" . h(replace_empty_string($event['event_venue'])) . "</td>";
				// foreach ($congregations as $congregation) {
				// 	if ($event['host_congregation_id'] === $congregation['congregation_id']) {
				// 		echo "<td>" . h($congregation['congregation_name']) . "</td>";
				// 	}
				// }
				echo "<td>" . h($event['event_begin_datetime']) . "</td>";
				echo "</tr>";
			}
		?>
	</table>
	<br>

</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
