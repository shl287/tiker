<?php 
	require_once('../../../private/initialize.php');

	//initialize event info
	$event = [];
	$event['event_name'] = $_POST['event_name'] ?? '';
	$event['event_venue'] = $_POST['event_venue'] ?? '';
	$event['host_congregation_id'] = $_POST['host_congregation_id'] ?? '';
	$event['event_begin_datetime'] = $_POST['event_begin_datetime'] ??  strtotime(date('Y-m-d'));
	$event['event_begin_date_day'] = $_POST['event_begin_date_day'] ?? intval(date('d'));
	$event['event_begin_date_month'] = $_POST['event_begin_date_month'] ?? intval(date('m'));
	$event['event_begin_date_year'] = $_POST['event_begin_date_year'] ?? intval(date('Y'));
	$event['event_begin_hour'] = $_POST['event_begin_hour'] ?? intval(date('H'));
	$event['event_begin_minute'] = $_POST['event_begin_minute'] ?? intval(date('i'));

	//get all congregation info from db for drop down list display
	$congregations = find_all_congregations();

	if (is_post_request()) {
		$validForm = true;
		if (stringIsBlank($event['event_name'])) {
			$errors['has_blank_event_name'] = 'event name cannot be blank';
			$validForm = false;
		} 
		if (stringIsBlank($event['event_venue'])) {
			$errors['has_blank_event_venue'] = 'event venue cannot be blank';
			$validForm = false;
		}
		if(!checkdate($event['event_begin_date_month'], $event['event_begin_date_day'], $event['event_begin_date_year'])) {
			$errors['has_invalid_datetime'] = 'event begin time must be a valid date';
			$validForm = false;
		} else {
			$event['event_begin_datetime'] = mktime($event['event_begin_hour'], $event['event_begin_minute'], 0, $event['event_begin_date_month'], $event['event_begin_date_day'], $event['event_begin_date_year']);
			if ($event['event_begin_datetime'] <= strtotime("today")){
				$errors['has_past_event_begintime'] = 'event begin time must be in a future date';
				$validForm = false;
			}
		}
		// submit for db update if form validation pass 
		if ($validForm) {
			// insert person info to db
			$event['event_begin_datetime'] = date('Y-m-d H:i:s', $event['event_begin_datetime']);
			$result = insert_event($event);
			if ($result === true) {
				$event_id = mysqli_insert_id($db);
				redirect_to(url_for('/admin/event/show_event.php?event_id=' . h(u($event_id))));
			} else {
				$errors = $result;
			}
		}
	}

	$page_title = 'Create Event';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">
	<form action="<?php echo url_for('/admin/event/new_event.php') ?>" method="post">
		<h4>Register New Event</h4>
		<br>

		<?php echo display_errors($errors) ?>
    <div class="form-group">
      <label>Event Name:</label>
      <input type="text" class="form-control" name="event_name" value="<?php echo h($event['event_name']); ?>">
    </div>

    <div class="form-group">
      <label>Venue:</label>
      <input type="text" class="form-control" name="event_venue" value="<?php echo h($event['event_venue']); ?>">
    </div>

    <div class="form-group">
			<label>Congregation</label>
			<select class="form-control" name="host_congregation_id">
				<?php 
					foreach ($congregations as $congregation) {
						echo "<option value=\"" . $congregation['congregation_id'] . "\"";
						if ($event['host_congregation_id'] == $congregation['congregation_id']) {
							echo "selected";
						}
						echo ">" . $congregation['congregation_name'] . "</option>";
					}
				?>
			</select>
		</div>

    <div class="form-group">
    	<div class="row">
				<div class="col-sm-12">
					<label>Date & Time (dd-mmm-yyyy hh:mm)</label>
				</div><!-- col -->
    	</div><!-- row -->

    	<div class="row">
				<div class="col-md-2">
					<select class="form-control" name="event_begin_date_day">
						<?php 
							for ($i=1; $i <= 31; $i++) { 
								echo "<option value=\"" . $i . "\"";
								if ($i == $event['event_begin_date_day']) {
									echo "selected";
								}
								echo ">$i</option>";
							}
						?>
					</select>
				</div><!-- col -->   		
				<div class="col-md-2">
					<select class="form-control" name="event_begin_date_month">
						<?php 
							for ($i=1; $i <= 12; $i++) { 
								echo "<option value=\"" . $i . "\"";
								if ($i == $event['event_begin_date_month']) {
									echo "selected";
								}
								echo ">" . date("M", mktime(0, 0, 0, $i, 1)) . "</option>";
							}
						?>
					</select>
				</div><!-- col -->   		
				<div class="col-md-2">
					<select class="form-control" name="event_begin_date_year">
						<!-- TODO: maintain user selection on post -->
						<option value="<?php echo date('Y'); ?>"
							<?php
								if (intval(date('Y')) == intval($event['event_begin_date_year'])) {
									echo "selected";
								}
							?>
							><?php echo date('Y');?></option>
						<option value="<?php echo date('Y') + 1; ?>"
							<?php 
								if (intval(date('Y')) + 1 == intval($event['event_begin_date_year'])) {
									echo "selected";
								}
							?>
								><?php echo intval(date('Y')) + 1;?></option>
					</select>
				</div><!-- col -->   		
				<div class="col-md-2">
					<select class="form-control" name="event_begin_hour">
						<?php 
							for ($i=0; $i <= 23; $i++) { 
								echo "<option value=\"" . $i . "\"";
								if ($i == 10) {
									echo "selected";
								}
								echo ">$i</option>";
							}
						?>
					</select>
				</div><!-- col -->   		
				<div class="col-md-2">
					<select class="form-control" name="event_begin_minute">
						<?php 
							for ($i=0; $i < 4; $i++) { 
								echo "<option value=\"" . $i*15 . "\"";
								if ($i * 15 == 0) {
									echo "selected";
								}
								echo ">". $i * 15 . "</option>";
							}
						?>
					</select>
				</div><!-- col -->  
    	</div><!-- row -->
    </div><!-- form-group -->
    <br>

		<input type="submit" value="Register" class="btn btn-primary btn-block btn-sm">
	</form>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
