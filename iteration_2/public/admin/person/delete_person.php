<?php 
	require_once('../../../private/initialize.php');

	if (!isset($_GET['person_id'])) {
		redirect_to(url_for('/admin/person/person_index_admin_view.php'));
	}

	$person_id = $_GET['person_id'] ?? $_POST['person_id'];
	$person = find_person_by_id($person_id);
	$congregation = find_congregation_by_id($person['reg_congregation_id']);
	$congregation_name = $congregation['congregation_name'];

	//deal with form submission
	if(is_post_request()) {
		$result = delete_person_by_id($person_id);
		if ($result === true) {
			redirect_to(url_for('/admin/person/person_index_admin_view.php'));
		}
	}

	$page_title = 'De-register Person';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">

	<h4>Are you sure you want to delete this person?</h4>
	<br>	

	<dl>
		<dt>Full Name</dt>
		<dd><?php echo h(replace_empty_string($person['full_name'])); ?></dd>
	</dl>
	<dl>
		<dt>Prefered Name</dt>
		<dd><?php echo h(replace_empty_string($person['prefered_name'])); ?></dd>
	</dl>
	<dl>
		<dt>Gender</dt>
		<dd><?php echo get_gender_txt(h($person['gender'])); ?>
		</dd>
	</dl>
	<dl>
		<dt>Spiritual Status</dt>
		<dd><?php echo get_spiritual_status_txt(h($person['spiritual_status'])); ?>
		</dd>
	</dl>
	<dl>
		<dt>Congregation</dt>
		<dd><?php echo h(replace_empty_string($congregation_name)); ?>
		</dd>
	</dl>
	<br>

	<form action="<?php echo url_for('/admin/person/delete_person.php?person_id=' . h(u($person_id))) ?>" method="post">
		<input type="submit" name="commit" value="Delete" class="btn btn-primary btn-sm">
	</form>
	<br>
	
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
