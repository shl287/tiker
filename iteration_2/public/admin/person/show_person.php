<?php 
	require_once('../../../private/initialize.php');

	$person_id = $_GET['person_id'] ?? '0';
	$person = find_person_by_id($person_id);
	$congregation = find_congregation_by_id($person['reg_congregation_id']);
	$congregation_name = $congregation['congregation_name'];

	$page_title = 'Show Person';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">

	<h4>Person Detail</h4>
	<br>

	<dl>
		<dt>Full Name</dt>
		<dd><?php echo h(replace_empty_string($person['full_name'])); ?></dd>
	</dl>
	<dl>
		<dt>Prefered Name</dt>
		<dd><?php echo h(replace_empty_string($person['prefered_name'])); ?></dd>
	</dl>
	<dl>
		<dt>Gender</dt>
		<dd><?php echo get_gender_txt(h($person['gender'])); ?>
		</dd>
	</dl>
	<dl>
		<dt>Spiritual Status</dt>
		<dd><?php echo get_spiritual_status_txt(h($person['spiritual_status'])); ?>
		</dd>
	</dl>
	<dl>
		<dt>Congregation</dt>
		<dd><?php echo h(replace_empty_string($congregation_name)); ?>
		</dd>
	</dl>
	<br>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
