<?php 
	require_once('../../../private/initialize.php');

	//initialize person info
	$person = [];
	$person['full_name'] = $_POST['full_name'] ?? '';
	$person['prefered_name'] = $_POST['prefered_name'] ?? '';
	$person['gender'] = $_POST['gender'] ?? '';
	$person['spiritual_status'] = $_POST['spiritual_status'] ?? '';
	$person['reg_congregation_id'] = $_POST['reg_congregation_id'] ?? '';

	//get all congregation info from db for drop down list display
	$congregations = find_all_congregations();

	//deal with form submission
	if(is_post_request()) {
		//check full name is not blank
		if (stringIsBlank($person['prefered_name'])) {
			$errors['has_blank_prefered_name'] = 'prefered name cannot be blank';
		} if (!empty(find_person_by_prefered_name_and_reg_congregation($person['prefered_name'], $person['reg_congregation_id']))) {
			$errors['has_duplicating_prefered_name'] = 'prefered name already exists in this congregation';
		}
		else {
			//insert person info to db
			$result = insert_person($person);
			if ($result === true) {
				$person_id = mysqli_insert_id($db);
				redirect_to(url_for('/admin/person/show_person.php?person_id=' . h(u($person_id))));
			} else {
				$errors = $result;
			}
		}
	}

	$page_title = 'Register Person';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">
	<h4>Register Person</h4>
	<br>

	<form action="<?php echo url_for('/admin/person/new_person.php') ?>" method="post">
		<?php echo display_errors($errors) ?>

    <div class="form-group">
      <label>Full Name:</label>
      <input type="text" class="form-control" name="event_name" value="<?php echo h($person['full_name']); ?>">
    </div>

    <div class="form-group">
      <label>Prefered Name:</label>
      <input type="text" class="form-control" name="prefered_name" value="<?php echo h($person['prefered_name']); ?>">
    </div>

    <div class="form-group">
      <label>Gender:</label>
			<select class="form-control" name="gender">
				<?php 
					foreach ($person_gender_map as $key => $value) {
						echo "<option value=\"" . $key . "\"";
						if ($person['gender'] == $key) {
							echo "selected";
						}
						echo ">" . $value . "</option>";
					}
				?>
			</select>
    </div>

    <div class="form-group">
      <label>Spiritual Status:</label>
			<select class="form-control" name="spiritual_status">
				<?php 
					foreach ($person_spiritual_status_map as $key => $value) {
						echo "<option value=\"" . $key . "\"";
						if ($person['spiritual_status'] == $key) {
							echo "selected";
						}
						echo ">" . $value . "</option>";
					}
				?>
			</select>
    </div>

    <div class="form-group">
      <label>Congregation:</label>
			<select class="form-control" name="reg_congregation_id">
				<?php 
					foreach ($congregations as $congregation) {
						echo "<option value=\"" . $congregation['congregation_id'] . "\"";
						if ($person['reg_congregation_id'] == $congregation['congregation_id']) {
							echo "selected";
						}
						echo ">" . $congregation['congregation_name'] . "</option>";
					}
				?>
			</select>
    </div>
    <br>

		<input type="Submit" class="btn btn-primary btn-block btn-sm">
	</form>
	<br>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
