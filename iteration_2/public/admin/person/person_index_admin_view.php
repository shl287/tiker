<?php 
	require_once('../../../private/initialize.php');

	//get all congregation info from db for drop down list display
	$congregations = find_all_congregations();

	$persons = find_all_persons();

	$page_title = 'Person';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">
	<h4>Person &nbsp;<a href="<?php echo url_for('/admin/person/new_person.php') ?>"><button class="btn btn-primary btn-sm">Register New Person</button></a></h4>
	<br>

	<table class="table	table-striped table-condensed table-bordered bg-basic">
		<tr>
			<!-- <th>ID</th> -->
			<th>&nbsp;</th>
			<!-- <th>Full Name</th> -->
			<th>Prefered Name</th>
			<th>Gender</th>
			<th>Spirituality</th>
			<th>Congregation</th>
		</tr>
		<?php 
			foreach ($persons as $person) {
				echo "<tr>";
				echo "<td><a href=\"" . url_for('/admin/person/show_person.php?person_id=') . h(u($person['person_id'])) . "\"><button class=\"btn btn-primary btn-sm\">View</button></a>";
				echo "		<a href=\"" . url_for('/admin/person/edit_person.php?person_id=') . h(u($person['person_id'])) . "\"><button class=\"btn btn-primary btn-sm\">Edit</button></a>";
				echo "		<a href=\"" . url_for('/admin/person/delete_person.php?person_id=') . h(u($person['person_id'])) . "\"><button class=\"btn btn-primary btn-sm\">Delete</button></a></td>";
				// echo "<td>" . h($person['person_id']) . "</td>";
				// echo "<td>" . h(replace_empty_string($person['full_name'])) . "</td>";
				echo "<td>" . h(replace_empty_string($person['prefered_name'])) . "</td>";
				echo "<td>" . h($person_gender_map[$person['gender']]) . "</td>";
				echo "<td>" . h($person_spiritual_status_map[$person['spiritual_status']]) . "</td>";

				foreach ($congregations as $congregation) {
					if ($person['reg_congregation_id'] === $congregation['congregation_id']) {
						echo "<td>" . h($congregation['congregation_name']) . "</td>";
					}
				}
				echo "</tr>";
			}
		?>
	</table>
	<br>

</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
