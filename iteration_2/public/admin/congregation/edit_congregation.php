<?php 
	require_once('../../../private/initialize.php');

	if(!isset($_GET['congregation_id'])) {
		redirect_to(url_for('/admin/congregation/congregation_index.php'));
	}
	$congregation_id = $_GET['congregation_id'];
	$congregation = [];

	//deal with form submission
	if(is_post_request()) {
		//initialize congregation info
		$congregation['congregation_id'] = $congregation_id;
		$congregation['congregation_name'] = $_POST['congregation_name'] ?? '';

		// check full sname is not blank
		if (stringIsBlank($congregation['congregation_name'])) {
			$errors['has_blank_congregation_name'] = 'congregation name cannot be blank';
		} else {

			//update congregation info to db
			$result = update_congregation($congregation);
			if ($result === true) {
				redirect_to(url_for('/admin/congregation/congregation_index.php'));
			} else {
				$errors = $result;
			}
		}
	} else  {
		// first time load page, get current congregation info from db
		$congregation = find_congregation_by_id($congregation_id);
	}

	$page_title = 'Edit Congregation';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">
	<form class="form" action="<?php echo url_for('/admin/congregation/edit_congregation.php?congregation_id=' . h(u($congregation_id))); ?>" method="post">

		<h4>Edit Congregation Info</h4>
		<br>
		
		<?php echo display_errors($errors); ?>
    <div class="form-group">
      <label>Congregation Name:</label>
      <input type="text" class="form-control" name="congregation_name" value="<?php echo h(replace_empty_string($congregation['congregation_name'])); ?>">
    </div>
		<br>

		<div id="operation">
			<input type="Submit" class="btn btn-primary btn-sm">
		</div>
		<br>

	</form>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
