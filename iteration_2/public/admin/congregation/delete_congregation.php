<?php 
	require_once('../../../private/initialize.php');

	if (!isset($_GET['congregation_id'])) {
		redirect_to(url_for('/admin/congregation/congregation_index.php'));
	}

	$congregation_id = $_GET['congregation_id'] ?? $_POST['congregation_id'];
	$congregation = find_congregation_by_id($congregation_id);

	//deal with form submission
	if(is_post_request()) {
		$result = delete_congregation_by_id($congregation_id);
		if ($result === true) {
			redirect_to(url_for('/admin/congregation/congregation_index.php'));
		}
	}

	$page_title = 'Delete Congregation';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">

		<h4>Are you sure you want to delete this congregation?</h4>
		<br>

		<dl>
			<dt>Congregation Name</dt>
			<dd><?php echo h($congregation['congregation_name']); ?></dd>
		</dl>
		<br>
		
		<form action="<?php echo url_for('/admin/congregation/delete_congregation.php?congregation_id=') . h(u($congregation_id)) ?>" method="post">
			<div id="operation">
				<input type="submit" class="btn btn-primary btn-sm" name="commit" value="Delete">
			</div>
		</form>
		<br>

</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
