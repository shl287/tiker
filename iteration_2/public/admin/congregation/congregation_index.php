<?php 
	require_once('../../../private/initialize.php');

	//get all congregation info from db for drop down list display
	$congregations = find_all_congregations();

	$page_title = 'Congregation';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">
	<h4>Congregation List &nbsp; <a href="<?php echo url_for('/admin/congregation/new_congregation.php') ?>"><button type="button" class="btn btn-primary btn-sm">Register New Congregation</button></a></h4>
	<br>

	<table class="table table-striped table-condensed table-bordered bg-basic">
		<tr class="tr">
			<th>&nbsp;</th>
			<th>Congregation Name</th>
		</tr>
		<?php 
			foreach ($congregations as $congregation) {
				echo "	<tr>";
				// echo "<td>" . $congregation['congregation_id'] . "</td>";
				echo "		<td><a href=\"" . url_for('/admin/congregation/edit_congregation.php?congregation_id=') . h(u($congregation['congregation_id'])) . "\"><button type=\"button\" class=\"btn btn-primary btn-sm\">Edit</button></a>";
				echo "				<a href=\"" . url_for('/admin/congregation/delete_congregation.php?congregation_id=') . h(u($congregation['congregation_id'])) . "\"><button type=\"button\" class=\"btn btn-primary btn-sm\">Delete</button></a></td>";
				echo "		<td>" . $congregation['congregation_name'] . "</td>";
				echo "	</tr>";
			}
		?>
	</table>
	<br>

</div><!-- container-->

<?php include(SHARED_PATH . '/footer.php'); ?>
