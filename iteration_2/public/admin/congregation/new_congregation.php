<?php 
	require_once('../../../private/initialize.php');

	//initialize congregation info
	$congregation = [];
	$congregation['congregation_name'] = $_POST['congregation_name'] ?? '';

	//deal with form submission
	if(is_post_request()) {
		//check full name is not blank
		if (stringIsBlank($congregation['congregation_name'])) {
			$errors['has_blank_congregation_name'] = 'congregation name cannot be blank';
		} else {
			//insert person info to db
			$result = insert_congregation($congregation);
			if ($result === true) {
				$congregation_id = mysqli_insert_id($db);
				redirect_to(url_for('/admin/congregation/congregation_index.php'));
			} else {
				$errors = $result;
			}
		}
	}

	$page_title = 'Create Congregation';
	include(SHARED_PATH . '/header.php'); 
?>

<div class="container-fluid bg-light">
	<form action="<?php echo url_for('/admin/congregation/new_congregation.php'); ?>" method="post">

		<h4>Create Congregation</h4>
		<br>

		<?php echo display_errors($errors) ?>
    <div class="form-group">
      <label>Congregation Name:</label>
      <input type="text" class="form-control" name="congregation_name" value="<?php echo h($congregation['congregation_name']); ?>">
    </div>
		<br>

		<div id="operation">
			<input type="Submit" class="btn btn-primary btn-sm">
		</div>
		<br>
	</form>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
