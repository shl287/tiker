<?php 

	ob_start();//output buffering turned on

	define("PRIVATE_PATH", dirname(__FILE__));
	define("PROJECT_PATH", dirname(PRIVATE_PATH));
	define("PUBLIC_PATH", PROJECT_PATH . '/guest');
	define("SHARED_PATH", PRIVATE_PATH . '/shared');

	define("LOCALHOST_ROOT", '/tiker/iteration_2/public');

	define("SYSTEM_NAME", "Tiker");
	define("BUILD_NAME", "v2 Beta");

	require_once('functions.php');
	require_once('database.php');
	require_once('query_functions.php');
	require_once('code_conversion_functions.php');
	require_once('validation_functions.php');

	//reflects the dbms comment on gender column in person table
	//naming convention: table_column_map
	$person_gender_map = ['0' => 'Male', '1' => 'Female', '2' => 'Other'];
	$person_spiritual_status_map = ['0' => 'Baptized', '1' => 'Gold Seeker', '2' => 'Other'];
	$attendance_attendance_code_map = ['0' => 'U', '1' => 'O', '2' => 'L', '3' => 'A', '4' => '?'];

	$db = db_connect();
  $errors = [];
	
?>