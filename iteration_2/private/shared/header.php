<?php  
  if(!isset($page_title)) { 
    $page_title = BUILD_NAME;
  }
?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <title><?php echo SYSTEM_NAME . ' - ' . h($page_title); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  </head>
  <body class="container-fluid">
    <nav class="navbar navbar-light bg-light">
  		<h2 class=""><?php echo SYSTEM_NAME . ' ' . BUILD_NAME?></h1>
      <a href="<?php echo url_for('/index.php'); ?>"><button type="button" class="btn btn-primary btn-sm">Home</button></a>
  	</nav>
    <br>
