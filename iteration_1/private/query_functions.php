<?php

	function find_all_congregations() {
		global $db;

		$sql = "SELECT * FROM congregation";

		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);

		$congregations = [];
		while($congregation = mysqli_fetch_assoc($result_set)) {
			$congregations[] = $congregation;
		}
		mysqli_free_result($result_set);

		return $congregations;
	}

	function find_congregation_by_id($id) {
		global $db;

		$sql = "SELECT * FROM congregation ";
		$sql .= "WHERE congregation_id = '" . db_escape($db, $id) . "'";

		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);
		$congregation = mysqli_fetch_assoc($result_set);
		mysqli_free_result($result_set);
		return $congregation;
	}

	function find_host_congregation_by_event_id($event_id) {
		global $db;

		//find host_congregation_id by event_id
		$sql = "SELECT * FROM event ";
		$sql .= "WHERE event_id = '" . db_escape($db, $event_id) . "'";
		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);
		$event = mysqli_fetch_assoc($result_set);
		mysqli_free_result($result_set);

		//find host congregation
		$host_congregation = find_congregation_by_id($event['host_congregation_id']);
		return $host_congregation;
	}

	function insert_congregation($congregation) {
		global $db;

		$sql = "INSERT INTO congregation (";
		$sql .= "congregation_name";
		$sql .= ") VALUES (";
		$sql .= "'" . db_escape($db, $congregation['congregation_name']) . "'";
		$sql .= ")";

		$result = mysqli_query($db, $sql);

		if ($result) {
			return true;
		} else {
			echo mysqli_error($db);
			db_disconnect();
			exit();
		}
	}

	function update_congregation($congregation) {
		global $db;

		$sql = "UPDATE congregation SET ";
		$sql .= "congregation_name = '" . db_escape($db, $congregation['congregation_name']) . "' ";
    $sql .= "WHERE congregation_id = '" . db_escape($db, $congregation['congregation_id']) . "' ";
    $sql .= "LIMIT 1;";

		$result = mysqli_query($db, $sql);

		if ($result) {
			return true;
		} else {
			echo mysqli_error($db);
			db_disconnect();
			exit();
		}
	}

	function delete_congregation_by_id($congregation_id) {
		global $db;
		
		//delete attendance, person, event associated with the congregation
		delete_event_by_host_congregation_id($congregation_id);
		delete_person_by_reg_congregation_id($congregation_id);

		//delete the congregation
		$sql = "DELETE FROM congregation ";
		$sql .= "WHERE congregation_id ='" . db_escape($db, $congregation_id) . "' ";
		$sql .= "LIMIT 1";

		$result = mysqli_query($db, $sql);
		if (!$result) {
			echo mysqli_error($db);
			db_disconnect();
			exit;	
		} else {	
			return true;
		}
	}

	function find_all_persons() {
		global $db;

		$sql = "SELECT * FROM person ORDER BY reg_congregation_id";

		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);
		$persons = [];
		while($person = mysqli_fetch_assoc($result_set)) {
			$persons[] = $person;
		}
		mysqli_free_result($result_set);

		return $persons;
	}

	function find_person_by_id($id) {
		global $db;

		$sql = "SELECT * FROM person ";
		$sql .= "WHERE person_id = '" . db_escape($db, $id) . "'";

		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);
		$person = mysqli_fetch_assoc($result_set);
		mysqli_free_result($result_set);
		return $person;
	}

	function find_all_person_attendance_by_event($event_id, $visitor_flag) {
		global $db;

		$host_congregation = find_host_congregation_by_event_id($event_id);
		$host_congregation_id = $host_congregation['congregation_id'];

		//find all regular attendees and their attendance in the event
		$sql = "SELECT p1.person_id, p1.full_name, p1.prefered_name, 
						p1.reg_congregation_id, a1.event_id, a1.attendance_code FROM person p1 ";
		$sql .= "LEFT OUTER JOIN attendance a1 ON ";
		$sql .= "p1.person_id = a1.person_id ";
		$sql .= "AND a1.event_id = '" . db_escape($db, $event_id) . "' ";
		if ($visitor_flag) {
			//look for visitors
			$sql .= "WHERE p1.reg_congregation_id <> '" . db_escape($db, $host_congregation_id) . "' ";
		} else {
			//look for regular attendees
			$sql .= "WHERE p1.reg_congregation_id = '" . db_escape($db, $host_congregation_id) . "' ";
		}
		$sql .= "ORDER BY p1.reg_congregation_id ";

		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);
		$persons = [];
		while($person = mysqli_fetch_assoc($result_set)) {
			$persons[] = $person;
		}
		mysqli_free_result($result_set);
		return $persons;
	}

	function find_all_persons_attendance_by_event($event_id) {
		global $db;

		$sql = "SELECT p.reg_congregation_id, p.prefered_name, a.attendance_code FROM person p, attendance a ";
		$sql .= "WHERE a.person_id = p.person_id ";
		$sql .= "AND a.event_id = '" . db_escape($db, $event_id) . "' ";
		$sql .= "AND a.attendance_code in (1,2,3)";

		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);
		$persons = [];
		while($person = mysqli_fetch_assoc($result_set)) {
			$persons[] = $person;
		}
		mysqli_free_result($result_set);
		return $persons;
	}

	function insert_person($person) {
		global $db;

		$sql = "INSERT INTO PERSON (";
		$sql .= "full_name, prefered_name, ";
		$sql .= "gender, spiritual_status, reg_congregation_id ";
		$sql .= ") VALUES (";
		$sql .= "'" . db_escape($db, $person['full_name']) . "', ";
		$sql .= "'" . db_escape($db, $person['prefered_name']) . "', ";
		$sql .= "'" . db_escape($db, $person['gender']) . "', ";
		$sql .= "'" . db_escape($db, $person['spiritual_status']) . "', ";
		$sql .= "'" . db_escape($db, $person['reg_congregation_id']) . "'";
		$sql .= ")";

		// echo $sql;

		$result = mysqli_query($db, $sql);

		if ($result) {
			return true;
		} else {
			echo mysqli_error($db);
			db_disconnect();
			exit();
		}
	}

	function update_person($person) {
		global $db;

		$sql = "UPDATE PERSON SET ";
		$sql .= "full_name = '" . db_escape($db, $person['full_name']) . "', ";
		$sql .= "prefered_name = '" . db_escape($db, $person['prefered_name']) . "', ";
		$sql .= "gender = '" . db_escape($db, $person['gender']) . "', ";
		$sql .= "spiritual_status = '" . db_escape($db, $person['spiritual_status']) . "', ";
		$sql .= "reg_congregation_id = '" . db_escape($db, $person['reg_congregation_id']) . "' ";
    $sql .= "WHERE person_id = '" . db_escape($db, $person['person_id']) . "' ";
    $sql .= "LIMIT 1;";

		$result = mysqli_query($db, $sql);
		if ($result) {
			return true;
		} else {
			echo mysqli_error($db);
			db_disconnect();
			exit();
		}
	}

	function delete_person_by_id($person_id) {
		
		//delete attendance record related to the person
		$result = delete_attendance_by_person_id($person_id);

		if ($result) {
			global $db;

			$sql = "DELETE FROM person ";
			$sql .= "WHERE person_id='" . db_escape($db, $person_id) . "' ";
			$result = mysqli_query($db, $sql);
			if ($result) {
				return true;
			} else {
				echo mysqli_error($db);
				db_disconnect();
				exit;	
			}
		}
	}

	function delete_person_by_reg_congregation_id ($reg_congregation_id) {

		//find all persons registered with the congregation
		global $db;
		$sql = "SELECT * FROM person ";
		$sql .= "WHERE reg_congregation_id = '" . db_escape($db, $reg_congregation_id) . "'";
		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);

		//delete attendance related to the persons
		while($person = mysqli_fetch_assoc($result_set)) {
			$result = delete_attendance_by_person_id($person['person_id']);
		}
		mysqli_free_result($result_set);

		//delete persons related to the host congregation
		//the last call in delete_attendance_by_person_id() may exit with result == false;
		if ($result) {
			global $db;
			$sql = "DELETE FROM person ";
			$sql .= "WHERE reg_congregation_id='" . db_escape($db, $reg_congregation_id) . "'";
			$result = mysqli_query($db, $sql);
			if ($result) {
				return true;
			} else {	
				echo mysqli_error($db);
				db_disconnect();
				exit;	
			}
		}
	}

	function find_all_events() {
		global $db;

		$sql = "SELECT * FROM event ORDER BY event_begin_datetime DESC, host_congregation_id ASC";

		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);

		$events = [];
		while($event = mysqli_fetch_assoc($result_set)) {
			$events[] = $event;
		}
		mysqli_free_result($result_set);

		return $events;
	}

	function find_event_by_id($id) {
		global $db;

		$sql = "SELECT * FROM event ";
		$sql .= "WHERE event_id = '" . db_escape($db, $id) . "'";

		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);
		$result = mysqli_fetch_assoc($result_set);
		mysqli_free_result($result_set);
		return $result;
	}

	function insert_event($event) {
		global $db;

		$sql = "INSERT INTO event (";
		$sql .= "event_name, host_congregation_id, event_begin_datetime, event_venue";
		$sql .= ") VALUES (";
		$sql .= "'" . db_escape($db, $event['event_name']) . "', ";
		$sql .= "'" . db_escape($db, $event['host_congregation_id']) . "', ";
		$sql .= "'" . db_escape($db, $event['event_begin_datetime']) . "', ";
		$sql .= "'" . db_escape($db, $event['event_venue']) . "' ";
		$sql .= ")";

		$result = mysqli_query($db, $sql);

		if ($result) {
			return true;
		} else {
			echo mysqli_error($db);
			db_disconnect();
			exit();
		}
	}

	function update_event($event) {
		global $db;

		$sql = "UPDATE event SET ";
		$sql .= "event_name = '" . db_escape($db, $event['event_name']) . "', ";
		$sql .= "host_congregation_id = '" . db_escape($db, $event['host_congregation_id']) . "', ";
		$sql .= "event_begin_datetime = '" . db_escape($db, $event['event_begin_datetime']) . "', ";
		$sql .= "event_venue = '" . db_escape($db, $event['event_venue']) . "' ";
    $sql .= "WHERE event_id = '" . db_escape($db, $event['event_id']) . "' ";
    $sql .= "LIMIT 1;";

		$result = mysqli_query($db, $sql);

		if ($result) {
			return true;
		} else {
			echo mysqli_error($db);
			db_disconnect();
			exit();
		}
	}

	function delete_event_by_id($event_id) {

		//delete attendance record related to the person
		$result = delete_attendance_by_event_id($event_id);

		if ($result) {
			global $db;
			$sql = "DELETE FROM event ";
			$sql .= "WHERE event_id='" . db_escape($db, $event_id) . "' ";
			$sql .= "LIMIT 1";
			$result = mysqli_query($db, $sql);
			if ($result) {
				return true;
			} else {	
				echo mysqli_error($db);
				db_disconnect();
				exit;	
			}
		}
	}

	function delete_event_by_host_congregation_id ($host_congregation_id) {

		//find all events hosted by the congregation
		global $db;
		$sql = "SELECT * FROM event ";
		$sql .= "WHERE host_congregation_id = '" . db_escape($db, $host_congregation_id) . "'";
		$result_set = mysqli_query($db, $sql);
		confirm_result_set($result_set, $sql);

		//delete attendance related to the events
		while($event = mysqli_fetch_assoc($result_set)) {
			$result = delete_attendance_by_event_id($event['event_id']);
		}
		mysqli_free_result($result_set);

		//delete events related to the host congregation
		//the last call in delete_attendance_by_event_id() may exit with result == false;
		if ($result) {
			global $db;
			$sql = "DELETE FROM event ";
			$sql .= "WHERE host_congregation_id='" . db_escape($db, $host_congregation_id) . "'";
			$result = mysqli_query($db, $sql);
			if ($result) {
				return true;
			} else {	
				echo mysqli_error($db);
				db_disconnect();
				exit;	
			}
		}
	}

	function insert_attendance($attendance) {
		global $db;

		$sql = "INSERT INTO attendance (";
		$sql .= "event_id, person_id, attendance_code, comment";
		$sql .= ") VALUES (";
		$sql .= "'" . db_escape($db, $attendance['event_id']) . "', ";
		$sql .= "'" . db_escape($db, $attendance['person_id']) . "', ";
		$sql .= "'" . db_escape($db, $attendance['attendance_code']) . "', ";
		$sql .= "'" . db_escape($db, $attendance['comment']) . "' ";
		$sql .= ")";

		$result = mysqli_query($db, $sql);

		if ($result) {
			return true;
		} else {
			echo mysqli_error($db);
			db_disconnect();
			return false;
			// exit();
		}
	}

	function delete_attendance_by_person_id($person_id) {
		global $db;

		$sql = "DELETE FROM attendance ";
		$sql .= "WHERE person_id='" . db_escape($db, $person_id) . "' ";
		$result = mysqli_query($db, $sql);
		
		if ($result) {
			return true;
		} else {	
			echo mysqli_error($db);
			db_disconnect();
			exit;	
		}
	}

	function delete_attendance_by_event_id($event_id) {
		global $db;

		$sql = "DELETE FROM attendance ";
		$sql .= "WHERE event_id='" . db_escape($db, $event_id) . "' ";
		$result = mysqli_query($db, $sql);
		
		if ($result) {
			return true;
		} else {	
			echo mysqli_error($db);
			db_disconnect();
			exit;	
		}
	}
?>