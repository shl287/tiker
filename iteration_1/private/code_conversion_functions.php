<?php 

	function get_gender_txt ($gender_code) {
		global $person_gender_map;
		return $person_gender_map[$gender_code];
	} 

	function get_spiritual_status_txt ($spiritual_status_code) {
		global $person_spiritual_status_map;
		return $person_spiritual_status_map[$spiritual_status_code];
	}

?>