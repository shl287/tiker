-- initial congregation
insert into congregation (congregation_name) values ('City');
insert into congregation (congregation_name) values ('East');
insert into congregation (congregation_name) values ('West');
insert into congregation (congregation_name) values ('Lighthouse');
insert into congregation (congregation_name) values ('non-GAC');

-- initial person, Smith family
insert into person (
	full_name, prefered_name, 
	gender, spiritual_status, reg_congregation_id) 
	values (
	'John Smith', 'Uncle John',
	0, 0, 1
);
insert into person (
	full_name, prefered_name, 
	gender, spiritual_status, reg_congregation_id) 
	values (
	'Rose Smith', 'Auntie Rose',
	1, 0, 1
);
insert into person (
	full_name, prefered_name, 
	gender, spiritual_status, reg_congregation_id) 
	values (
	'Geogre Smith', 'Geogre',
	0, 1, 1
);
insert into person (
	full_name, prefered_name, 
	gender, spiritual_status, reg_congregation_id) 
	values (
	'Jennifer Smith', 'Jennifer',
	1, 1, 1
);


-- initial event
insert into event (
	event_name, 
	host_congregation_id, event_begin_datetime, event_venue
	) VALUES (
	'GACC Sabbath worship program (1-Sep-2018)',
	1, '2018-9-1 10:00:00', 'Clayton Religious Centre'
);

insert into event (
	event_name, 
	host_congregation_id, event_begin_datetime, event_venue
	) VALUES (
	'GACE Sabbath worship program (1-Sep-2018)',
	2, '2018-9-1 10:00:00', 'Clayton Religious Centre'
);

insert into event (
	event_name, 
	host_congregation_id, event_begin_datetime, event_venue
	) VALUES (
	'GACW Sabbath worship program (1-Sep-2018)',
	3, '2018-9-1 10:00:00', 'Clayton Religious Centre'
);

insert into event (
	event_name, 
	host_congregation_id, event_begin_datetime, event_venue
	) VALUES (
	'GACL Sabbath worship program (1-Sep-2018)',
	4, '2018-9-1 10:00:00', 'Clayton Religious Centre'
);


-- initial attendance record
insert into attendance (
	event_id, person_id, attendance_code, comment
	) VALUES (
	1, 1, 2, "sick at home"
);

insert into attendance (
	event_id, person_id, attendance_code, comment
	) VALUES (
	1, 2, 0, ""
);

insert into attendance (
	event_id, person_id, attendance_code, comment
	) VALUES (
	1, 3, 0, ""
);

insert into attendance (
	event_id, person_id, attendance_code, comment
	) VALUES (
	1, 4, 1, "went back to get sabbath school materials"
);
