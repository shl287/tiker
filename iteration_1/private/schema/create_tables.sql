
-- person table
drop table congregation;

create table congregation (
	congregation_id int(11) not null auto_increment,
	congregation_name varchar(50) not null,
	primary key (congregation_id)
);


-- person table and index
alter table person drop index fk_person_reg_congregation_id;
drop table person;

create table person (
	person_id int(11) not null auto_increment,
	full_name varchar(50) not null,
	prefered_name varchar(50),
	gender tinyint(1) not null comment '0:MALE; 1:FEMALE; 2:OTHER', 
	spiritual_status tinyint(1) not null comment '0:baptized; 1:seeker_gold; 2:other',
	reg_congregation_id int(11),
	primary key(person_id)
);

alter table person add index fk_person_reg_congregation_id (reg_congretation_id);



-- event table and index
alter table event drop index fk_event_host_congregation_id;
drop table event;

create table event(
	event_id int(11) not null auto_increment,
	event_name varchar(50) not null,
	host_congregation_id int(11) not null comment 'multi-congregation event will have 1 main host congregation',
	event_begin_datetime datetime not null comment 'planned begin datetime',
	event_venue varchar(50),
	primary key(event_id)
);

alter table event add index fk_event_host_congregation_id (host_congregation_id);


-- attendance table and index
alter table attendance drop index fk_attendance_event_id;
alter table attendance drop index fk_attendance_person_id;
drop table attendance;

create table attendance (
	event_id int(11) not null,
	person_id int(11) not null,
	attendance_code tinyint(1) not null default 0 comment '0:un-recorded; 1:ontime; 2:late; 3:absent; 4:unknown',
	comment varchar(2000),
	primary key(event_id,person_id)
);

alter table attendance add index fk_attendance_event_id (event_id);

alter table attendance add index fk_attendance_person_id (person_id);
