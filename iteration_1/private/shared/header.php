<?php  
  if(!isset($page_title)) { 
    $page_title = 'ticker';
  }
?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <title>ticker - <?php echo h($page_title); ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" media="all" href="<?php echo url_for('/stylesheets/ticker.css'); ?>">
  </head>
  <body>
    <header>
  		<h1>ticker iteration 1</h1>
  	</header>

  	<navigation>
  		<ul>
  			<li><a href="<?php echo url_for('/index.php'); ?>">Main Menu</a></li>
  		</ul>
  	</navigation>