<?php require_once('../private/initialize.php'); ?>

<?php $page_title = 'Guest Menu'; ?>

<?php include(SHARED_PATH . '/header.php'); ?>

<div id="content">
	<div id="main-menu">
		<h2>Main Menu</h2>
			<ul>
				<li><a href="<?php echo url_for('/admin/admin_index.php'); ?>"><button>admin area</button></a></li>
			</ul>
			<ul>
				<li><a href="<?php echo url_for('/guest/guest_index.php'); ?>"><button>guest area</button></a></li>
			</ul>
	</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
