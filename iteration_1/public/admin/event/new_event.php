<?php 
	require_once('../../../private/initialize.php');

	//initialize event info
	$event = [];
	$event['event_name'] = $_POST['event_name'] ?? '';
	$event['event_venue'] = $_POST['event_venue'] ?? '';
	$event['host_congregation_id'] = $_POST['host_congregation_id'] ?? '';
	$event['event_begin_datetime'] = $_POST['event_begin_datetime'] ??  strtotime(date('Y-m-d'));
	$event['event_begin_date_day'] = $_POST['event_begin_date_day'] ?? intval(date('d'));
	$event['event_begin_date_month'] = $_POST['event_begin_date_month'] ?? intval(date('m'));
	$event['event_begin_date_year'] = $_POST['event_begin_date_year'] ?? intval(date('Y'));
	$event['event_begin_hour'] = $_POST['event_begin_hour'] ?? intval(date('H'));
	$event['event_begin_minute'] = $_POST['event_begin_minute'] ?? intval(date('i'));

	//get all congregation info from db for drop down list display
	$congregations = find_all_congregations();

	if (is_post_request()) {
		//debug
		// echo print_r($event) . "<br>";
		// echo "today int = " . mktime(0, 0, 0, $event['event_begin_date_month'], $event['event_begin_date_day'], $event['event_begin_date_year']) . "<br>";
		// echo "tomorrow = " . date('Y-m-d',strtotime("tomorrow")) . "<br>";
		// echo "tomorrow int = " . strtotime("tomorrow");

		$validForm = true;
		if (stringIsBlank($event['event_name'])) {
			$errors['has_blank_event_name'] = 'event name cannot be blank';
			$validForm = false;
		} 
		if (stringIsBlank($event['event_venue'])) {
			$errors['has_blank_event_venue'] = 'event venue cannot be blank';
			$validForm = false;
		}
		if(!checkdate($event['event_begin_date_month'], $event['event_begin_date_day'], $event['event_begin_date_year'])) {
			$errors['has_invalid_datetime'] = 'event begin time must be a valid date';
			$validForm = false;
		} else {
			$event['event_begin_datetime'] = mktime($event['event_begin_hour'], $event['event_begin_minute'], 0, $event['event_begin_date_month'], $event['event_begin_date_day'], $event['event_begin_date_year']);
			if ($event['event_begin_datetime'] <= strtotime("today")){
				$errors['has_past_event_begintime'] = 'event begin time must be in a future date';
				$validForm = false;
			}
		}
		// submit for db update if form validation pass 
		if ($validForm) {
			// insert person info to db
			$event['event_begin_datetime'] = date('Y-m-d H:i:s', $event['event_begin_datetime']);
			$result = insert_event($event);
			if ($result === true) {
				$event_id = mysqli_insert_id($db);
				redirect_to(url_for('/admin/event/show_event.php?event_id=' . h(u($event_id))));
			} else {
				$errors = $result;
			}
		}
	}

	$page_title = 'Create Event';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

	<a class="back-link" href="<?php echo url_for('/admin/event/event_index.php')?>"> &laquo; Back to Event Menu</a><br>

	<div id="main-menu">
		<form action="<?php echo url_for('/admin/event/new_event.php') ?>" method="post">
			<h2>Register Event</h2>

			<?php echo display_errors($errors) ?>
			<dl>
				<dt>Event Name</dt>
				<dd><input type="text" name="event_name" value="<?php echo h($event['event_name']); ?>"></dd>
			</dl>
			<dl>
				<dt>Venue</dt>
				<dd><input type="text" name="event_venue" value="<?php echo h($event['event_venue']); ?>"></dd>
			</dl>

			<dl>
				<dt>Congregation</dt>
				<dd>
					<select name="host_congregation_id">
						<?php 
							foreach ($congregations as $congregation) {
								echo "<option value=\"" . $congregation['congregation_id'] . "\"";
								if ($event['host_congregation_id'] == $congregation['congregation_id']) {
									echo "selected";
								}
								echo ">" . $congregation['congregation_name'] . "</option>";
							}
						?>
					</select>
				</dd>
			</dl>

			<dl>
				<dt>Date & Time</dt>
				<dd>
					<select name="event_begin_date_day">
						<?php 
							for ($i=1; $i <= 31; $i++) { 
								echo "<option value=\"" . $i . "\"";
								if ($i == $event['event_begin_date_day']) {
									echo "selected";
								}
								echo ">$i</option>";
							}
						?>
					</select>

					<select name="event_begin_date_month">
						<?php 
							for ($i=1; $i <= 12; $i++) { 
								echo "<option value=\"" . $i . "\"";
								if ($i == $event['event_begin_date_month']) {
									echo "selected";
								}
								echo ">" . date("M", mktime(0, 0, 0, $i, 1)) . "</option>";
							}
						?>
					</select>

					<select name="event_begin_date_year">
						<!-- TODO: maintain user selection on post -->
						<option value="<?php echo date('Y'); ?>"
							<?php
								if (intval(date('Y')) == intval($event['event_begin_date_year'])) {
									echo "selected";
								}
							?>
							><?php echo date('Y');?></option>
						<option value="<?php echo date('Y') + 1; ?>"
							<?php 
								if (intval(date('Y')) + 1 == intval($event['event_begin_date_year'])) {
									echo "selected";
								}
							?>
								><?php echo intval(date('Y')) + 1;?></option>
					</select>
					<select name="event_begin_hour">
						<?php 
							for ($i=0; $i <= 23; $i++) { 
								echo "<option value=\"" . $i . "\"";
								if ($i == 10) {
									echo "selected";
								}
								echo ">$i</option>";
							}
						?>
					</select>

					<select name="event_begin_minute">
						<?php 
							for ($i=0; $i < 4; $i++) { 
								echo "<option value=\"" . $i*15 . "\"";
								if ($i * 15 == 0) {
									echo "selected";
								}
								echo ">". $i * 15 . "</option>";
							}
						?>
					</select>
				</dd>
			</dl>
		</div>
		<div id="operation">
			<input type="submit" value="Create">
		</div>
	</form>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
