<?php 
	require_once('../../../private/initialize.php');

	$events = find_all_events();
	$congregations = find_all_congregations();

	$page_title = 'Event Management';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

<a class="back-link" href="<?php echo url_for('/admin/admin_index.php')?>"> &laquo; Back to Admin Menu</a><br>

	<div id="main-menu">
		<h2>Event</h2>

		<div class="action">
			<a href="<?php echo url_for('/admin/event/new_event.php') ?>"><button>Create new event</button></a>
		</div>

		<br>
		<table class="list">
			<tr>
				<th>&nbsp;</th>
				<!-- <th>Event ID</th> -->
				<th>Event Name</th>
				<th>Venue</th>
				<th>Host Congregation</th>
				<th>Event Datetime</th>
			</tr>
			<?php 
				foreach ($events as $event) {
					echo "<tr>";
					echo "<td><a href=\"" . url_for('/admin/event/show_event.php?event_id=') . h(u($event['event_id'])) . "\"><button>View</button></a>";
					echo "		<a href=\"" . url_for('/admin/event/edit_event.php?event_id=') . h(u($event['event_id'])) . "\"><button>Edit</button></a>";
					echo "		<a href=\"" . url_for('/admin/event/delete_event.php?event_id=') . h(u($event['event_id'])) . "\"><button>Delete</button></a></td>";
					// echo "<td>" . h($event['event_id']) . "</td>";
					echo "<td>" . h($event['event_name']) . "</td>";
					echo "<td>" . h($event['event_venue']) . "</td>";
					foreach ($congregations as $congregation) {
						if ($event['host_congregation_id'] === $congregation['congregation_id']) {
							echo "<td>" . h($congregation['congregation_name']) . "</td>";
						}
					}
					echo "<td>" . h($event['event_begin_datetime']) . "</td>";
					echo "</tr>";
				}
			?>
		</table>
	</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
