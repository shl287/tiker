<?php 
	require_once('../../../private/initialize.php');

	$event_id = $_GET['event_id'] ?? '0';
	$event = find_event_by_id($event_id);
	$congregation = find_congregation_by_id($event['host_congregation_id']);
	$congregation_name = $congregation['congregation_name'];

	$page_title = 'Show Event';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

	<a class="back-link" href="<?php echo url_for('/admin/event/event_index.php')?>"> &laquo; Back to Event Menu</a><br>

	<div id="main-menu">
		<h2>Event Detail</h2>
		<dl>
			<dt>Event Name</dt>
			<dd><?php echo h($event['event_name']); ?></dd>
		</dl>
		<dl>
			<dt>Venue</dt>
			<dd><?php echo h($event['event_venue']); ?>
			</dd>
		</dl>
		<dl>
			<dt>Host</dt>
			<dd><?php echo h($congregation_name); ?>
			</dd>
		</dl>
		<dl>
			<dt>Date & Time</dt>
			<dd><?php echo h($event['event_begin_datetime']); ?>
			</dd>
		</dl>			
		</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
