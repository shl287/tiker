<?php require_once('../../private/initialize.php'); ?>

<?php $page_title = 'Admin Menu'; ?>

<?php include(SHARED_PATH . '/header.php'); ?>

<div id="content">

<a class="back-link" href="<?php echo url_for('/index.php')?>"> &laquo; Back to Main Menu</a><br>

	<div id="main-menu">
		<h2>Admin Menu</h2>
			<ul>
				<li><a href="<?php echo url_for('/admin/congregation/congregation_index.php'); ?>"><button>manage congregation</button></a></li>
			</ul>
			<ul>
				<li><a href="<?php echo url_for('/admin/event/event_index.php'); ?>"><button>manage event</button></a></li>
			</ul>
			<ul>
				<li><a href="<?php echo url_for('/admin/person/person_index.php'); ?>"><button>manage person</button></a></li>
			</ul>
	</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
