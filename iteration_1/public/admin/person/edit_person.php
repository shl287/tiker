<?php 
	require_once('../../../private/initialize.php');

	if(!isset($_GET['person_id'])) {
		redirect_to(url_for('/admin/person/person_index.php'));
	}
	$person_id = $_GET['person_id'];
	$person = [];

	//deal with form submission
	if(is_post_request()) {
		//initialize person info
		$person['person_id'] = $person_id;
		$person['full_name'] = $_POST['full_name'] ?? '';
		$person['prefered_name'] = $_POST['prefered_name'] ?? '';
		$person['gender'] = $_POST['gender'] ?? '';
		$person['spiritual_status'] = $_POST['spiritual_status'] ?? '';
		$person['reg_congregation_id'] = $_POST['reg_congregation_id'] ?? '';

		// check full sname is not blank
		if (stringIsBlank($person['prefered_name'])) {
			$errors['has_blank_prefered_name'] = 'prefered name cannot be blank';
		} else {

			//update person info to db
			$result = update_person($person);
			if ($result === true) {
				redirect_to(url_for('/admin/person/show_person.php?person_id=' . h(u($person_id))));
			} else {
				$errors = $result;
			}
		}
	} else  {
		// first time load page, get current person info from db
		$person = find_person_by_id($person_id);
	}

	//get all congregation info from db for drop down list display
	$congregations = find_all_congregations();

	$page_title = 'Edit Person';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

	<a class="back-link" href="<?php echo url_for('/admin/person/person_index.php')?>"> &laquo; Back to Person Menu</a><br>

	<div id="main-menu">
		<form action="<?php echo url_for('/admin/person/edit_person.php?person_id=' . h(u($person_id))); ?>" method="post">
			<h2>Edit Person</h2>

			<?php echo display_errors($errors) ?>
				<dl>
					<dt>Full Name</dt>
					<dd><input type="text" name="full_name" value="<?php echo h($person['full_name']); ?>"></dd>
				</dl>
				<dl>
					<dt>Prefered Name</dt>
					<dd><input type="text" name="prefered_name" value="<?php echo h($person['prefered_name']); ?>"></dd>
				</dl>
				<dl>
					<dt>Gender</dt>
					<dd>
						<select name="gender">
							<?php 
								foreach ($person_gender_map as $key => $value) {
									echo "<option value=\"" . $key . "\"";
									if ($person['gender'] == $key) {
										echo "selected";
									}
									echo ">" . $value . "</option>";
								}
							?>
						</select>
					</dd>
				</dl>
				<dl>
					<dt>Spiritual Status</dt>
					<dd>
						<select name="spiritual_status">
							<?php 
								foreach ($person_spiritual_status_map as $key => $value) {
									echo "<option value=\"" . $key . "\"";
									if ($person['spiritual_status'] == $key) {
										echo "selected";
									}
									echo ">" . $value . "</option>";
								}
							?>
						</select>
					</dd>
				</dl>
				<dl>
					<dt>Congregation</dt>
					<dd>
						<select name="reg_congregation_id">
							<?php 
								foreach ($congregations as $congregation) {
									echo "<option value=\"" . $congregation['congregation_id'] . "\"";
									if ($person['reg_congregation_id'] == $congregation['congregation_id']) {
										echo "selected";
									}
									echo ">" . $congregation['congregation_name'] . "</option>";
								}
							?>
						</select>
					</dd>
				</dl>
		</div>
		<div id="operation">
			<input type="Submit">
		</div>
	</form>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
