<?php 
	require_once('../../../private/initialize.php');

	//get all congregation info from db for drop down list display
	$congregations = find_all_congregations();

	$persons = find_all_persons();

	$page_title = 'Person';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

<a class="back-link" href="<?php echo url_for('/admin/admin_index.php')?>"> &laquo; Back to Admin Menu</a><br>

	<div id="main-menu">
		<h2>Person</h2>

		<div class="action">
			<a href="<?php echo url_for('/admin/person/new_person.php') ?>"><button>Register new person</button></a>
		</div>

		<br>
		<table class="list">
			<tr>
				<!-- <th>ID</th> -->
				<th>&nbsp;</th>
				<th>Full Name</th>
				<th>Prefered Name</th>
				<th>Gender</th>
				<th>Spirituality</th>
				<th>Church</th>
			</tr>
			<?php 
				foreach ($persons as $person) {
					echo "<tr>";
					echo "<td><a href=\"" . url_for('/admin/person/show_person.php?person_id=') . h(u($person['person_id'])) . "\"><button>View</button></a>";
					echo "		<a href=\"" . url_for('/admin/person/edit_person.php?person_id=') . h(u($person['person_id'])) . "\"><button>Edit</button></a>";
					echo "		<a href=\"" . url_for('/admin/person/delete_person.php?person_id=') . h(u($person['person_id'])) . "\"><button>De-register</button></a></td>";
					// echo "<td>" . h($person['person_id']) . "</td>";
					echo "<td>" . h($person['full_name']) . "</td>";
					echo "<td>" . h($person['prefered_name']) . "</td>";
					echo "<td>" . h($person_gender_map[$person['gender']]) . "</td>";
					echo "<td>" . h($person_spiritual_status_map[$person['spiritual_status']]) . "</td>";

					foreach ($congregations as $congregation) {
						if ($person['reg_congregation_id'] === $congregation['congregation_id']) {
							echo "<td>" . h($congregation['congregation_name']) . "</td>";
						}
					}
					echo "</tr>";
				}
			?>
		</table>
	</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
