<?php 
	require_once('../../../private/initialize.php');

	if (!isset($_GET['congregation_id'])) {
		redirect_to(url_for('/admin/congregation/congregation_index.php'));
	}

	$congregation_id = $_GET['congregation_id'] ?? $_POST['congregation_id'];
	$congregation = find_congregation_by_id($congregation_id);

	//deal with form submission
	if(is_post_request()) {
		$result = delete_congregation_by_id($congregation_id);
		if ($result === true) {
			redirect_to(url_for('/admin/congregation/congregation_index.php'));
		}
	}

	$page_title = 'Delete Congregation';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

	<a class="back-link" href="<?php echo url_for('/admin/congregation/congregation_index.php')?>"> &laquo; Back to Congregation Menu</a><br>

	<div id="main-menu">

		<h2>Are you sure you want to delete this congregation?</h2>
		<dl>
			<dt>Congregation Name</dt>
			<dd><?php echo h($congregation['congregation_name']); ?></dd>
		</dl>

		<form action="<?php echo url_for('/admin/congregation/delete_congregation.php?congregation_id=') . h(u($congregation_id)) ?>" method="post">
			<div id="operation">
				<input type="submit" name="commit" value="delete">
			</div>
		</form>

	</div>

</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
