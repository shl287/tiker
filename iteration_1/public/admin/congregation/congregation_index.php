<?php 
	require_once('../../../private/initialize.php');

	//get all congregation info from db for drop down list display
	$congregations = find_all_congregations();

	$page_title = 'Congregation';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

	<a class="back-link" href="<?php echo url_for('/admin/admin_index.php')?>"> &laquo; Back to Admin Menu</a><br>

	<div id="main-menu">
		<h2>Congregation</h2>

		<div class="action">
			<a href="<?php echo url_for('/admin/congregation/new_congregation.php') ?>"><button>Create new congregation</button></a>
		</div>

		<br>
		<table class="list">
			<tr>
				<!-- <th>ID</th> -->
				<th>&nbsp;</th>
				<th>Name</th>
			</tr>
			<?php 
				foreach ($congregations as $congregation) {
					echo "	<tr>";
					// echo "<td>" . $congregation['congregation_id'] . "</td>";
					echo "		<td><a href=\"" . url_for('/admin/congregation/edit_congregation.php?congregation_id=') . h(u($congregation['congregation_id'])) . "\"><button>Edit</button></a>";
					echo "				<a href=\"" . url_for('/admin/congregation/delete_congregation.php?congregation_id=') . h(u($congregation['congregation_id'])) . "\"><button>Delete</button></a></td>";
					echo "		<td>" . $congregation['congregation_name'] . "</td>";
					echo "	</tr>";
				}
			?>
		</table>
	</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
