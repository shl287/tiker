<?php require_once('../../private/initialize.php'); ?>

<?php $page_title = 'Guest Menu'; ?>

<?php include(SHARED_PATH . '/header.php'); ?>

<div id="content">

<a class="back-link" href="<?php echo url_for('/index.php')?>"> &laquo; Back to Main Menu</a><br>

	<div id="main-menu">
		<h2>Guest Menu</h2>
			<ul>
				<li><a href="<?php echo url_for('/guest/view_event_list.php'); ?>"><button>record attendance</button></a></li>
			</ul>
			<ul>
				<li><a href="<?php echo url_for('/guest/analyse_attendance.php'); ?>"><button>analyse attendance</button></a></li>
			</ul>
	</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
