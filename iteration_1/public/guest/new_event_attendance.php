<?php 
	require_once('../../private/initialize.php');

	if (!isset($_GET['event_id'])) {
		redirect_to(url_for('/guest/choose_event_attendance.php')); 
	}

	$event_id = $_GET['event_id'];
	$event = find_event_by_id($_GET['event_id']);
	$host_congregation = find_congregation_by_id($event['host_congregation_id']);
	$host_congregation_name = $host_congregation['congregation_name'];

	//insert attendance record for one person
	if (isset($_GET['person_id']) && isset($_GET['attendance_code'])) {
		//update attenance info in db before retrieving from db
		$attenance = [];
		$attenance['event_id'] = $event_id;
		$attenance['person_id'] = $_GET['person_id'];
		$attenance['attendance_code'] = $_GET['attendance_code'];
		$attenance['comment'] = '';
		if(!insert_attendance($attenance)) {
			//if refresh page with duplicating insert parameter, redirect to the same page
			redirect_to(url_for('/guest/new_event_attendance.php?event_id=' . $event_id)); 
		}
	}

	if (isset($_GET['no_more_visitor'])) {
		//insert attendance records for all remaining visitors when click no more visitor button
		$visitors = find_all_person_attendance_by_event($event_id, true);
		foreach ($visitors as $visitor) {
			if ($visitor['attendance_code'] != '1' && $visitor['attendance_code'] != '2') {
				$attenance = [];
				$attenance['event_id'] = $event_id;
				$attenance['person_id'] = $visitor['person_id'];
				$attenance['attendance_code'] = 4;
				$attenance['comment'] = '';
				insert_attendance($attenance);
			}
		}
	}

	$regular_attendees = find_all_person_attendance_by_event($event_id, false);
	$visitors = find_all_person_attendance_by_event($event_id, true);

	$has_visitor_attended = false;
	$has_visitor_not_set = false;
	foreach ($visitors as $visitor) {
		if (!empty($visitor['attendance_code']) && $visitor['attendance_code'] != '4') {
			$has_visitor_attended = true;
		}
		if (empty($visitor['attendance_code'])) {
		// if ($visitor['attendance_code'] != '1' 
		// 		&& $visitor['attendance_code'] != '2' 
		// 		&& $visitor['attendance_code'] != '4') {
			// $has_visitor_attendance_unknown = true;
			$has_visitor_not_set = true;
		} 
	}

	$page_title = 'Record Attendance';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

	<a class="back-link" href="<?php echo url_for('/guest/guest_index.php')?>"> &laquo; Back to Guest Menu</a><br>

	<div id="main-menu">
		<h2>Record Attendance</h2>

		<h3>Event Detail</h3>
		<table class="list">
			<tr>
				<!-- <th>Event ID</th> -->
				<th>Event Name</th>
				<th>Venue</th>
				<th>Host Congregation</th>
				<th>Date&Time</th>
			</tr>
			<tr>
				<!-- <td><?php echo h($event['event_id']); ?></td> -->
				<td><?php echo h($event['event_name']); ?></td>
				<td><?php echo h($event['event_venue']); ?></td>
				<td><?php echo h($host_congregation_name); ?></td>
				<td><?php echo h($event['event_begin_datetime']); ?></td>
			</tr>
		</table>

		<br>
		<h3>Record Regular Attendees</h3>
		<table class="list">
			<tr>
				<th>Attendance</th>
				<!-- <th>Person ID</th> -->
				<th>Full Name</th>
				<th>Prefered Name</th>
				<th>Regular Congregation</th>
			</tr>
			<?php 
				foreach ($regular_attendees as $regular_attendee) {
					echo "<tr>";
						echo "<td>";
						if (!isset($regular_attendee['attendance_code'])) {
							echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $regular_attendee['person_id'] . "&attendance_code=" . 1 . "\"><button>punctual</button></a> ";
							echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $regular_attendee['person_id'] . "&attendance_code=" . 2 . "\"><button>late</button></a> ";
							echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $regular_attendee['person_id'] . "&attendance_code=" . 3 . "\"><button>absent</button></a> ";
							// echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $regular_attendee['person_id'] . "&attendance_code=" . 4 . "\"><button>unknown 	</button></a> ";
						} 
						else {
							switch ($regular_attendee['attendance_code']) {
								case '1': echo "punctual"; break;
								case '2': echo "late"; break;
								case '3': echo "absent"; break;
								default: echo "unknown"; break;
							}
						}
						echo "</td>";
						// echo "<td>" . $regular_attendee['person_id'] . "</td>";
						echo "<td>" . $regular_attendee['full_name'] . "</td>";
						echo "<td>" . $regular_attendee['prefered_name'] . "</td>";
						$reg_congregation = find_congregation_by_id($regular_attendee['reg_congregation_id']);
						echo "<td>" . $reg_congregation['congregation_name'] . "</td>";
					echo "</tr>";
				}
			?>
		</table>

		<br>
		<h3>
			Record Visitors 
			<?php 
				if ($has_visitor_not_set) {
					echo "<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=' . $event_id . '&no_more_visitor=true') . "\"><button>No more visitor to record</button></a>";
				}
			?>
		</h3>

		<table class="list">
			<tr>
				<th>Attendance</th>
				<th>Full Name</th>
				<th>Prefered Name</th>
				<th>Regular Congregation</th>
			</tr>
			<?php 
				if (!$has_visitor_not_set && !$has_visitor_attended) {
					echo "<tr>";
					echo "<td>attendance record finished, no visitor has attended this event</td>";
					echo "<td></td>";
					echo "<td></td>";
					echo "<td></td>";
					echo "</tr>";
				} else {
					foreach ($visitors as $visitor) {
						if ($visitor['attendance_code'] != '4') {
							echo "<tr>";
							echo "<td>";
							if (!isset($visitor['attendance_code'])) {
								echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $visitor['person_id'] . "&attendance_code=" . '1' . "\"><button>punctual</button></a> ";
								echo "		<a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . $event_id . "&person_id=" . $visitor['person_id'] . "&attendance_code=" . '2' . "\"><button>late</button></a> ";
							} 
							else {
								switch ($visitor['attendance_code']) {
									case '1': echo "punctual"; break;
									case '2': echo "late"; break;
									default: echo "unknown"; break;
								}
							}
							echo "</td>";

							echo "<td>" . $visitor['full_name'] . "</td>";
							echo "<td>" . $visitor['prefered_name'] . "</td>";
							$reg_congregation = find_congregation_by_id($visitor['reg_congregation_id']);
							echo "<td>" . $reg_congregation['congregation_name'] . "</td>";
							echo "</tr>";
						}
					}
				}
			?>
		</table>

		<br>
		<br>
		</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
