<?php 
	require_once('../../private/initialize.php');

	$events = find_all_events();
	$congregations = find_all_congregations();

	$page_title = 'Event List';
	include(SHARED_PATH . '/header.php'); 
?>

<div id="content">

<a class="back-link" href="<?php echo url_for('/guest/guest_index.php')?>"> &laquo; Back to Guest Menu</a><br>

	<div id="main-menu">
		<h2>Choose an event to record attendance</h2>

		<table class="list">
			<tr>
				<th>&nbsp;</th>
				<!-- <th>ID</th> -->
				<th>Event Name</th>
				<th>Venue</th>
				<th>Host Congregation</th>
				<th>Event Datetime</th>
			</tr>
			<?php 
				foreach ($events as $event) {
					echo "<tr>";
					echo "<td><a href=\"" . url_for('/guest/new_event_attendance.php?event_id=') . h(u($event['event_id'])) . "\"><button>record attendance</button></a> ";
					// echo "<td>" . h($event['event_id']) . "</td>";
					echo "<td>" . h($event['event_name']) . "</td>";
					echo "<td>" . h($event['event_venue']) . "</td>";
					foreach ($congregations as $congregation) {
						if ($event['host_congregation_id'] === $congregation['congregation_id']) {
							echo "<td>" . h($congregation['congregation_name']) . "</td>";
						}
					}
					echo "<td>" . h($event['event_begin_datetime']) . "</td>";
					echo "</tr>";
				}
			?>
		</table>
	</div>
</div>

<?php include(SHARED_PATH . '/footer.php'); ?>
