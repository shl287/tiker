-------------- Executive Summary ----------------

The background

Gateway SDA church is operating in metropolitan Melbourne for many years and continues to expand. The church currently consists of 4 congregations and multiple care groups operates across suburbs in Melbourne.

The problem space

As a result of the expansion, reporting ministry activities accurately and in a timely manner is becoming a significant challenge. How to utilize the reported data to comprehend what's the best way to grow the ministry is another challenge that comes along with the growing ministry fields.

Intended audience

The intended audience of this project includes: 1) pastors and church elders who utilize reported insights to direct ministry activities; 2) bible workers who utilize the system to report attendance in events as well as notice trends in ministry activities; 3) regular church-participants who would like to help register the attendance situation in ministry events.

The solution

Project Ticker is aimed at streamlining and standardizing the collection of information regarding ministry activity attendance via an online platform, hence reduce the discrepancy of data and improve data accuracy. The data captured in the project can also be used for analysis purposes in the future to give insights on potential ministry growth areas and effective outreach approaches. 

Potential sponsors

The Gateway SDA church board is the potential sponsor