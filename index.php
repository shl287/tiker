<!DOCTYPE html>

<html lang="en">
  <head>
    <title>Tiker</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  </head>
  <body>
  	<div class="container-fluid bg-basic">
  		
  		<div class="row">
  			<div class="col-sm-12">
  				<br>
				</div><!-- row -->
			</div><!-- col -->

  		<div class="row">
  			<div class="col-sm-12">
					<a href="/tiker/iteration_1/public/index.php"><button type="button" class="btn btn-primary btn-sm">Go to Iteration 1</button></a>
				</div><!-- row -->
			</div><!-- col -->
			<br>

  		<div class="row">
  			<div class="col-sm-12">
					<a href="/tiker/iteration_2/public/index.php"><button type="button" class="btn btn-primary btn-sm">Go to Iteration 2</button></a>
				</div><!-- row -->
			</div><!-- col -->
			<br>
		</div><!-- container -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  </body>
</html>